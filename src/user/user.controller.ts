import {
  Controller,
  Get,
  Res,
  HttpStatus,
  Post,
  Body,
  Put,
  Patch,
  NotFoundException,
  Delete,
  Param,
  Query,
} from '@nestjs/common';
import { UserService } from './user.service';
import { CreateUserDto } from './dto/create-user.dto';
import { UpdateUserDto } from './dto/update-user.dto';
import { PaginationQueryDto } from 'src/common/dto/pagination-query.dto';

@Controller('users')
export class UserController {
  constructor(private readonly userService: UserService) {}

  @Post()
  async create(@Res() res, @Body() createUserDto: CreateUserDto) {
    try {
      const user = await this.userService.create(createUserDto);
      res.status(HttpStatus.OK).json({
        status: true,
        data: user,
        message: 'USERS-CREATED_SUCCESSFULLY',
      });
    } catch (error) {
      res.status(HttpStatus.BAD_REQUEST).json({
        status: false,
        data: null,
        message: error.message,
      });
    }
  }

  @Get()
  async findAll(@Res() res, @Query() paginationQuery: PaginationQueryDto) {
    try {
      const users = await this.userService.findAll(paginationQuery);
      console.log(users);
      res.status(HttpStatus.OK).json({
        status: true,
        data: users,
        message: 'USERS-FETCHED_SUCCESSFULLY',
      });
    } catch (error) {
      res.status(HttpStatus.BAD_REQUEST).json({
        status: false,
        data: null,
        message: error.message,
      });
    }
  }

  @Get('/:id')
  async findOne(@Res() res, @Param('id') userId: string) {
    try {
      const user = await this.userService.findOne(userId);

      res.status(HttpStatus.OK).json({
        status: true,
        data: user,
        message: 'USER-FETCHED_SUCCESSFULLY',
      });
    } catch (error) {
      res.status(HttpStatus.BAD_REQUEST).json({
        status: false,
        data: null,
        message: error.message,
      });
    }
  }

  @Patch('/:id')
  async update(
    @Res() res,
    @Param('id') userId: string,
    @Body() updateUserDto: UpdateUserDto,
  ) {
    try {
      const user = await this.userService.update(userId, updateUserDto);

      res.status(HttpStatus.OK).json({
        status: true,
        data: user,
        message: 'USER-UPDATED_SUCCESSFULLY',
      });
    } catch (error) {
      res.status(HttpStatus.BAD_REQUEST).json({
        status: false,
        data: null,
        message: error.message,
      });
    }
  }

  @Delete('/:id')
  async remove(@Res() res, @Param('id') userId: string) {
    try {
      const user = await this.userService.delete(userId);

      res.status(HttpStatus.OK).json({
        status: true,
        data: user,
        message: 'USER_DELETED_SUCCESSFULLY',
      });
    } catch (error) {
      res.status(HttpStatus.BAD_REQUEST).json({
        status: false,
        data: null,
        message: error.message,
      });
    }
  }
}
