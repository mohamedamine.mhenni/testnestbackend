import { Injectable, NotFoundException } from '@nestjs/common';
import { Model } from 'mongoose';
import { InjectModel } from '@nestjs/mongoose';
import { CreateUserDto } from './dto/create-user.dto';
import { UpdateUserDto } from './dto/update-user.dto';
import { User } from './entities/user.entity';
import { PaginationQueryDto } from '../common/dto/pagination-query.dto';
import { IUser } from './interfaces/user.interface';

@Injectable()
export class UserService {
  private searchFields: string[] = ['firstName', 'lastName'];

  constructor(
    @InjectModel(User.name)
    private readonly userModel: Model<User>,
  ) {}

  async findAll(paginationQuery: PaginationQueryDto): Promise<any> {
    const { searchTxt } = paginationQuery;

    const limit = paginationQuery.limit ? Number(paginationQuery.limit) : 10;
    const offset =
      paginationQuery.offset && paginationQuery.offset > 0
        ? (Number(paginationQuery.offset) - 1) * limit
        : 0;

    let where: any = {};
    let or: any[] = [];

    if (searchTxt) {
      this.searchFields.forEach((field_name) => {
        or = [
          ...or,
          {
            [field_name]: {
              $regex: searchTxt,
              $options: 'i',
            },
          },
        ];
      });
    }
    if (or?.length > 0) where = { $or: or };
    const users = await this.userModel
      .find(where)
      .skip(offset)
      .limit(limit)
      .exec();
    const totalCount = await this.userModel.countDocuments(where);

    return {
      users,
      totalCount,
      limit,
      offset: Number(paginationQuery.offset),
    };
  }

  async findOne(userId: string): Promise<User> {
    const user = await this.userModel.findById({ _id: userId }).exec();

    if (!user) {
      throw new NotFoundException(`User #${user} does not exist`);
    }

    return user;
  }
  async create(createUserDto: CreateUserDto): Promise<IUser> {
    const newUser = new this.userModel(createUserDto);
    return await newUser.save();
  }
  async update(userId: string, updateUserDto: UpdateUserDto): Promise<IUser> {
    const existingUser = await this.userModel.findByIdAndUpdate(
      { _id: userId },
      updateUserDto,
      { new: true },
    );

    if (!existingUser) {
      throw new NotFoundException(`User  #${userId} does not found`);
    }

    return existingUser;
  }

  public async delete(userId: string): Promise<IUser> {
    const deletedUser = await this.userModel.findByIdAndRemove(userId);
    return deletedUser;
  }
}
