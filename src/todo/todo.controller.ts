import {
  Controller,
  Get,
  Res,
  HttpStatus,
  Post,
  Body,
  Put,
  Patch,
  NotFoundException,
  Delete,
  Param,
  Query,
} from '@nestjs/common';
import { TodoService } from './todo.service';
import { CreateTodoDto } from './dto/create-todo.dto';
import { UpdateTodoDto } from './dto/update-todo.dto';
import { PaginationQueryDto } from 'src/common/dto/pagination-query.dto';

@Controller('todo')
export class TodoController {
  constructor(private readonly todoService: TodoService) {}

  @Post('/getMyTodos')
  async findUserTodos(
    @Res() res,
    @Body() Body,
    @Query() paginationQuery: PaginationQueryDto,
  ) {
    try {
      const users = await this.todoService.findAll(
        paginationQuery,
        Body.userId,
      );

      res.status(HttpStatus.OK).json({
        status: true,
        data: users,
        message: 'USER_TODOS_FETCHED_SUCCESSFULLY',
      });
    } catch (error) {
      res.status(HttpStatus.BAD_REQUEST).json({
        status: false,
        data: null,
        message: error.message,
      });
    }
  }

  @Post()
  async create(@Res() res, @Body() createTodoDto: CreateTodoDto) {
    try {
      const todo = await this.todoService.create(createTodoDto);
      res.status(HttpStatus.OK).json({
        status: true,
        data: todo,
        message: 'TODO_CREATED_SUCCESSFULLY',
      });
    } catch (error) {
      res.status(HttpStatus.BAD_REQUEST).json({
        status: false,
        data: null,
        message: error.message,
      });
    }
  }
  @Delete('/:id')
  async remove(@Res() res, @Param('id') todoId: string) {
    try {
      const todo = await this.todoService.delete(todoId);

      res.status(HttpStatus.OK).json({
        status: true,
        data: todo,
        message: 'TODO_DELETED_SUCCESSFULLY',
      });
    } catch (error) {
      res.status(HttpStatus.BAD_REQUEST).json({
        status: false,
        data: null,
        message: error.message,
      });
    }
  }
}
