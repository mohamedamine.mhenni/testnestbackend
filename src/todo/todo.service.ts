import { Injectable, NotFoundException } from '@nestjs/common';
import { Model } from 'mongoose';
import { InjectModel } from '@nestjs/mongoose';
import { CreateTodoDto } from './dto/create-todo.dto';
import { UpdateTodoDto } from './dto/update-todo.dto';
import { Todo } from './entities/todo.entity';
import { PaginationQueryDto } from '../common/dto/pagination-query.dto';
import { ITodo } from './interfaces/todo.interface';
@Injectable()
export class TodoService {
  private searchFields: string[] = ['title'];

  constructor(
    @InjectModel(Todo.name)
    private readonly todoModel: Model<Todo>,
  ) {}

  async findAll(paginationQuery: PaginationQueryDto, userId): Promise<Todo[]> {
    const { limit, offset, searchTxt } = paginationQuery;

    // for the right way the user Id will be taken from JWT Token, in this expamle it will passed in the body request

    let where: any = { owner: userId };
    let or: any[] = [];
    if (searchTxt) {
      this.searchFields.forEach((field_name) => {
        or = [
          ...or,
          {
            [field_name]: {
              $regex: searchTxt,
              $options: 'i',
            },
          },
        ];
      });
    }
    if (or?.length > 0) where = { ...where, $or: or };
    console.log(where);
    return await this.todoModel.find(where).skip(offset).limit(limit).exec();
  }

  async create(createTodoDto: CreateTodoDto): Promise<ITodo> {
    const newTodo = new this.todoModel(createTodoDto);
    return await newTodo.save();
  }

  public async delete(todoId: string): Promise<ITodo> {
    const deletedTodo = await this.todoModel.findByIdAndRemove(todoId);
    return deletedTodo;
  }
}
