import { MaxLength, IsNotEmpty, IsEmail, IsString } from 'class-validator';

export class CreateTodoDto {
  @IsString()
  @MaxLength(30)
  @IsNotEmpty()
  readonly title: string;
  @IsString()
  @MaxLength(30)
  @IsNotEmpty()
  readonly owner: string;
  @IsString()
  @MaxLength(30)
  @IsNotEmpty()
  readonly description: string;
}
