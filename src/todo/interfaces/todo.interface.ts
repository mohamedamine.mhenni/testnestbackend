import { Document } from 'mongoose';

export interface ITodo extends Document {
  readonly title: string;
  readonly owner: string;
  readonly description: string;
}
